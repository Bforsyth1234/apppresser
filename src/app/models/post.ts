export interface Post {
    content: string,
    featured_image_url: string,
    id: number
    title: string

}