import { Component, Input, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
// import Swiper core and required modules
import { SwiperComponent } from "swiper/angular";
import SwiperCore, { EffectCoverflow, Pagination } from "swiper";
import { ModalController } from '@ionic/angular';

import { Post } from '../../models/post';
import { PostpageComponent } from '../postpage/postpage.component'

SwiperCore.use([EffectCoverflow, Pagination]);
@Component({
  selector: 'app-coverflow-slides',
  template: `
  test
  <div >
  <swiper
  [effect]="'coverflow'"
  [grabCursor]="true"
  [centeredSlides]="true"
  [slidesPerView]="'auto'"
  [coverflowEffect]="{
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows: true
  }"
  [pagination]="true"
  class="mySwiper"
>
  <ng-template swiperSlide *ngFor="let post of posts" 
    ><img (click)="loadPost(post)" src={{post.featured_image_url}} /></ng-template>
  </swiper>
  </div>
  `,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./coverflow-slides.component.scss'],
})
export class CoverflowSlidesComponent implements OnInit {
  @Input() posts: Post[];
  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  async loadPost(post: Post) {
    const modal = await this.modalController.create({
      component: PostpageComponent,
      componentProps: {
        post: post
      }
    });
    return await modal.present();
  }

}
