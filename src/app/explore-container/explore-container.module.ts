import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExploreContainerComponent } from './explore-container.component';
import { CoverflowSlidesComponent } from '../components/coverflow-slides/coverflow-slides.component';
import { SwiperModule } from "swiper/angular";

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, SwiperModule],
  declarations: [ExploreContainerComponent, CoverflowSlidesComponent],
  exports: [ExploreContainerComponent]
})
export class ExploreContainerComponentModule {}
