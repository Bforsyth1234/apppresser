import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DataService } from '../services/data.service';
import { Post } from '../models/post';

@Component({
  selector: 'app-explore-container',
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
})
export class ExploreContainerComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject()
  public posts: Post[] = [];
  public loading: boolean = true;

  @Input() name: string;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getDataFromAppPresser();
  }

  getDataFromAppPresser() {
    this.dataService.getData()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        data.map(item => {
          this.posts.push({
            content: item.content.rendered,
            featured_image_url: item.featured_image_urls.large,
            id: item.id,
            title: item.title.rendered
          });
        });
        if(this.posts.length > 0) {
          this.loading = false;
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}


